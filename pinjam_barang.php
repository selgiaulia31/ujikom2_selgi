<?php
session_start();
if(!isset($_SESSION['username'])){
	die("<script>alert('silahkan login');
	document.location.href='hal_login.php' 
	</script>");
}
?>
<?php
include "koneksi.php";
$id_inventaris=$_GET['id_inventaris'];

$select=mysqli_query($connect,"select * from inventaris where id_inventaris='$id_inventaris'");
$data=mysqli_fetch_array($select);
?>

<?php
include "koneksi.php";
$query="select max(kode) as maxKode from peminjaman";
$hasil=mysqli_query($connect,$query);
$data = mysqli_fetch_array($hasil);
$idPinjam =$data['maxKode'];

$noUrut=(int) substr($idPinjam,5,5);
$noUrut++;

$char = "PJ";
$newID = $char.sprintf("%05s",$noUrut); 
?>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>UJIKOM INVENTARIS</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="tampilan/img/favicon.ico">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/bootstrap.min.css">
    <!-- font awesome CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/font-awesome.min.css">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/owl.carousel.css">
    <link rel="stylesheet" href="tampilan/css/owl.theme.css">
    <link rel="stylesheet" href="tampilan/css/owl.transitions.css">
    <!-- meanmenu CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/meanmenu/meanmenu.min.css">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/animate.css">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/normalize.css">
	<!-- wave CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/wave/waves.min.css">
    <link rel="stylesheet" href="tampilan/css/wave/button.css">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- Notika icon CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/notika-custom-icon.css">
    <!-- Data Table JS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/jquery.dataTables.min.css">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/main.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/style.css">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/responsive.css">
    <!-- modernizr JS
		============================================ -->
    <script src="tampilan/js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- Start Header Top Area -->
    <div class="header-top-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="logo-area">
                        <h2><font color="white"> <i class="notika-icon notika-app"></i> Inventarisir </font></h2>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="header-top-menu">
                        <ul class="nav navbar-nav notika-top-nav">
                            <li class="nav-item dropdown">
                                <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><span><i class="notika-icon notika-search"></i></span></a>
                                <div role="menu" class="dropdown-menu search-dd animated flipInX">
                                    <div class="search-input">
                                        <i class="notika-icon notika-left-arrow"></i>
                                        <input type="text" />
                                    </div>
                                </div>
                            </li>
                            </div>
            </div>
        </div>
    </div>
    <!-- End Header Top Area -->
    <!-- Mobile Menu start -->
    <div class="mobile-menu-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="mobile-menu">
                        <nav id="dropdown">
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Mobile Menu end -->
    <!-- Main Menu area start-->
    <div class="main-menu-area mg-tb-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul class="nav nav-tabs notika-menu-wrap menu-it-icon-pro">
                        <li class="active"><a href="u_inventarisir.php"><i class="notika-icon notika-house"></i> Inventarisir </a>
                        </li>
                        <li ><a href="u_peminjaman.php"><i class="notika-icon notika-mail"></i> Peminjaman</a>
                        </li>
                         <li><a data-toggle="tab" href="#UA"><i class="notika-icon notika-form"></i> User Account</a>
                        </li>
                    </ul>
                    <div class="tab-content custom-menu-content">
                        
                        <div id="UA" class="tab-pane notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="pengaturan_user.php">Pengaturan Akun</a>
                                </li>
                                <li><a href="logout.php">Logout</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<!-- Main Menu area End-->
	<!-- Breadcomb area Start-->
	
	<!-- Breadcomb area End-->
    <!-- Data Table area Start-->
    <div class="data-table-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="data-table-list"> 
                        <div class="basic-tb-hd">
                            <h2>Data Inventaris
<div class="col-lg-8 col-md-8 col-sm-6">
<div class="modals-default-cl">
											<div class="modal animated rubberBand" id="myModalsix" role="dialog">
                                    <div class="modal-dialog modals-default">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            </div>
                                    </div>
                                </div>
							</div>
						</div>
					<?php
						$select=mysqli_query($connect,"SELECT * FROM inventaris s left join jenis p on p.id_jenis=s.id_jenis 
																   left join ruang r on r.id_ruang=s.id_ruang
																   left join petugas k on k.id_petugas=s.id_petugas
																    WHERE id_inventaris = $id_inventaris");

									$select_jenis=mysqli_query($connect,"SELECT `id_jenis`,`nama_jenis` FROM `jenis`");
									$select_ruang=mysqli_query($connect,"SELECT `id_ruang`,`nama_ruang` FROM `ruang`");
									$select_petugas=mysqli_query($connect,"SELECT `id_petugas`,`nama_petugas` FROM `petugas`");
									$data=mysqli_fetch_array($select);
					?>
											<form action="simpan_pinjam2.php" method="post" enctype="form-horizontal form-label-left" >
											<div class="form-group">	
												<input value="<?php echo $data['id_inventaris'];?>" type="hidden" class="form-control" name="id_inventaris" readonly>		
											</div>
											<div class="form-group">			
												<label> Kode Peminjaman </label>
												<input value="<?php echo $newID; ?>" type="text" class="form-control" name="kd" readonly>		
											</div>
											<div class="form-group">			
												<label> Nama </label>
												<input value="<?php echo $data['nama'];?>" type="text" class="form-control" name="nama" readonly>		
											</div>
											<div class="form-group">			
												<label> kondisi </label>
												<input value="<?php echo $data['kondisi'];?>" type="text" class="form-control" name="kondisi" readonly>		
											</div>
											<div class="form-group">			
												<label> keterangan </label>
												<input value="<?php echo $data['keterangan'];?>" type="text" class="form-control" name="keterangan" readonly>		
											</div>
											<div class="form-group">			
												<label> jumlah Tersedia </label>
												<input value="<?php echo $data['jumlah'];?>" type="text" class="form-control" name="jumlah" readonly>		
											</div>
											<div class="form-group">			
												<label> kode inventaris </label>
												<input value="<?php echo $data['kode_inventaris'];?>" type="text" class="form-control" name="kode_inventaris" readonly>		
											</div>
											<div class="form-group">			
												<label> jumlah </label>
												<input  type="number" class="form-control" name="jml">		
											</div>
											<div class="form-group">
												<label class="hrzn-fm">petugas </label>
												<select name="id_pegawai" required class="select2_group form-control" readonly>
													<option value="">nama petugas</option>
														<?php
														include "../koneksi.php";
														$select=mysqli_query($connect,"select * from petugas where username='".$_SESSION['username']."'");
														while($data=mysqli_fetch_array($select)){
														?>
													<option value="<?php echo $data['id_petugas']; ?>"><?php echo $data['nama_petugas']; ?></option>
												<?php }?>
												</select>
											</div>
											<div class="form-group">
												<input class="btn-primary" type="submit" name="submit" value="masukan daftar pinjam" />
											</div>
										</form>

										</div>
									</div>
								</div>
							</div>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <div class="footer-copyright-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="footer-copy-right">
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- End Footer area-->
    <!-- jquery
		============================================ -->
    <script src="tampilan/js/vendor/jquery-1.12.4.min.js"></script>
    <!-- bootstrap JS
		============================================ -->
    <script src="tampilan/js/bootstrap.min.js"></script>
    <!-- wow JS
		============================================ -->
    <script src="tampilan/js/wow.min.js"></script>
    <!-- price-slider JS
		============================================ -->
    <script src="tampilan/js/jquery-price-slider.js"></script>
    <!-- owl.carousel JS
		============================================ -->
    <script src="tampilan/js/owl.carousel.min.js"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="tampilan/js/jquery.scrollUp.min.js"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="tampilan/js/meanmenu/jquery.meanmenu.js"></script>
    <!-- counterup JS
		============================================ -->
    <script src="tampilan/js/counterup/jquery.counterup.min.js"></script>
    <script src="tampilan/js/counterup/waypoints.min.js"></script>
    <script src="tampilan/js/counterup/counterup-active.js"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="tampilan/js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- sparkline JS
		============================================ -->
    <script src="tampilan/js/sparkline/jquery.sparkline.min.js"></script>
    <script src="tampilan/js/sparkline/sparkline-active.js"></script>
    <!-- flot JS
		============================================ -->
    <script src="tampilan/js/flot/jquery.flot.js"></script>
    <script src="tampilan/js/flot/jquery.flot.resize.js"></script>
    <script src="tampilan/js/flot/flot-active.js"></script>
    <!-- knob JS
		============================================ -->
    <script src="tampilan/js/knob/jquery.knob.js"></script>
    <script src="tampilan/js/knob/jquery.appear.js"></script>
    <script src="tampilan/js/knob/knob-active.js"></script>
    <!--  Chat JS
		============================================ -->
    <script src="tampilan/js/chat/jquery.chat.js"></script>
    <!--  todo JS
		============================================ -->
    <script src="tampilan/js/todo/jquery.todo.js"></script>
	<!--  wave JS
		============================================ -->
    <script src="tampilan/js/wave/waves.min.js"></script>
    <script src="tampilan/js/wave/wave-active.js"></script>
    <!-- plugins JS
		============================================ -->
    <script src="tampilan/js/plugins.js"></script>
    <!-- Data Table JS
		============================================ -->
    <script src="tampilan/js/data-table/jquery.dataTables.min.js"></script>
    <script src="tampilan/js/data-table/data-table-act.js"></script>
    <!-- main JS
		============================================ -->
    <script src="tampilan/js/main.js"></script>
	<!-- tawk chat JS
		============================================ -->
    <script src="tampilan/js/tawk-chat.js"></script>
</body>

</html>