<?php
session_start();
if(!isset($_SESSION['username'])){
	die("<script>alert('silahkan login');
	document.location.href='hal_login.php' 
	</script>");
}
?>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>UJIKOM INVENTARIS</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="tampilan/img/favicon.ico">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/bootstrap.min.css">
    <!-- font awesome CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/font-awesome.min.css">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/owl.carousel.css">
    <link rel="stylesheet" href="tampilan/css/owl.theme.css">
    <link rel="stylesheet" href="tampilan/css/owl.transitions.css">
    <!-- meanmenu CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/meanmenu/meanmenu.min.css">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/animate.css">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/normalize.css">
	<!-- wave CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/wave/waves.min.css">
    <link rel="stylesheet" href="tampilan/css/wave/button.css">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- Notika icon CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/notika-custom-icon.css">
    <!-- Data Table JS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/jquery.dataTables.min.css">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/main.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/style.css">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/responsive.css">
    <!-- modernizr JS
		============================================ -->
    <script src="tampilan/js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- Start Header Top Area -->
    <div class="header-top-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="logo-area">
                        <h2><font color="white"> <i class="notika-icon notika-app"></i> Inventarisir </font></h2>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="header-top-menu">
                        <ul class="nav navbar-nav notika-top-nav">
                            <li class="nav-item dropdown">
                                <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><span></span></a>
                                <div role="menu" class="dropdown-menu search-dd animated flipInX">
                                    <div class="search-input">
                                        <i class="notika-icon notika-left-arrow"></i>
                                        <input type="text" />
                                    </div>
                                </div>
                            </li>
						</ul>
                    </div>
            </div>
        </div>
    </div>
    <!-- End Header Top Area -->
    <!-- Mobile Menu start -->
    <div class="mobile-menu-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="mobile-menu">
                        <nav id="dropdown">
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Mobile Menu end -->
    <!-- Main Menu area start-->
    <div class="main-menu-area mg-tb-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul class="nav nav-tabs notika-menu-wrap menu-it-icon-pro">
                        <li class="active"><a href="inventarisir.php"><i class="notika-icon notika-house"></i> Inventarisir </a>
                        </li>
                        <li ><a href="peminjaman.php"><i class="notika-icon notika-mail"></i> Peminjaman</a>
                        </li>
                        <li ><a href="pengembalian.php"><i class="notika-icon notika-edit"></i> Pengembalian</a>
                        </li>
                        <li><a href="laporan.php"><i class="notika-icon notika-bar-chart"></i> Laporan Data </a>
                        </li>
                        <li ><a data-toggle="tab" href="#Referensi"><i class="notika-icon notika-windows"></i> Referensi</a>
                        </li>
                        <li><a data-toggle="tab" href="#UA"><i class="notika-icon notika-form"></i> User Account</a>
                        </li>
                    </ul>
                    <div class="tab-content custom-menu-content">
                                               <div id="DB" class="tab-pane in notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="inventarisir.php">Data Peminjaman</a>
                                </li>
                                </li>
                            </ul>
                        </div>
						<div id="PB" class="tab-pane in notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="peminjaman.php">Peminjaman</a>
                                </li>
                                </li>
                            </ul>
                        </div>
						<div id="KB" class="tab-pane in notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="pengembalian.php">Pengembalian</a>
                                </li>
                                </li>
                            </ul>
                        </div>
						<div id="Laporan" class="tab-pane in notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="inventarisir.php">Data Barang</a>
                                </li>
                                <li><a href="laporan_peminjaman.html">Data Peminjaman</a>
                                </li>
                                <li><a href="ua.html">User Akun</a>
                                </li>
                                </li>
                            </ul>
                        </div>
                        <div id="Referensi" class="tab-pane notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li class="active"><a href="jenis.php">Jenis</a></li>
								<li><a href="level.php">Level</a></li>
								<li><a href="ruang.php">Ruang</a></li>
								<li><a href="pegawai.php">Pegawai</a></li>
								<li><a href="petugas.php">Petugas</a></li>
                            </ul>
                        </div>
                        <div id="UA" class="tab-pane notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="pengaturan_admin.php">Pengaturan Akun</a>
                                </li>
                                <li><a href="logout.php">Logout</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Main Menu area End-->
	<!-- Breadcomb area Start-->
	
	<!-- Breadcomb area End-->
    <!-- Data Table area Start-->
    <div class="data-table-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="data-table-list"> 
                        <div class="basic-tb-hd">
                            <h2>Data Barang
											<div class="modals-default-cl">
											<button type="button" class="btn notika-btn-teal" data-toggle="modal" data-target="#myModalsix"><font color="white">Tambah Data</font></button>
											<div class="modal animated rubberBand" id="myModalsix" role="dialog">
                                    <div class="modal-dialog modals-default">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                                <h2>Tambah Data Barang</h2>
												<form action="simpan_db.php" method="post" class="form-example-int form-horizental mg-t-15">
													<div class="form-example-int form-horizental">
														<div class="form-group">
															<div class="row">
																<div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
																	<label class="hrzn-fm">Nama </label>
																</div>
																<div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
																	<div class="nk-int-st">
																		<input type="text" class="form-control input-sm" name="nama" placeholder="Nama Barang" required>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="form-example-int form-horizental mg-t-15">
														<div class="form-group">
															<div class="row">
																<div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
																	<label class="hrzn-fm">Kondisi </label>
																</div>
																<div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
																	<div class="nk-int-st">
																		<input type="text" class="form-control input-sm" name="kondisi" placeholder="Kondisi Barang">
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="form-example-int form-horizental">
														<div class="form-group">
															<div class="row">
																<div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
																	<label class="hrzn-fm">Keterangan </label>
																</div>
																<div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
																	<div class="nk-int-st">
																		<input type="text" class="form-control input-sm" name="keterangan" placeholder="Keterangan">
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="form-example-int form-horizental mg-t-15">
														<div class="form-group">
															<div class="row">
																<div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
																	<label class="hrzn-fm">Jumlah </label>
																</div>
																<div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
																	<div class="nk-int-st">
																		<input type="number" class="form-control input-sm" name="jumlah" placeholder="Jumlah Barang">
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="form-example-int form-horizental">
														<div class="form-group">
															<div class="row">
																<div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
																	<label class="hrzn-fm">Jenis </label>
																</div>
																<div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
																	<div class="nk-int-st">
																		<select name="id_jenis" required class="select2_group form-control">
																			<option value="">Pilih Jenis</option>
																			<?php
																				include "koneksi.php";
																				$select=mysqli_query($connect,"select * from jenis");
																				while($data=mysqli_fetch_array($select)){
																			?>
																			<option value="<?php echo $data['id_jenis'];?>"><?php echo $data['nama_jenis'];?></option>
																			<?php }?>
																		</select>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="form-example-int form-horizental mg-t-15">
														<div class="form-group">
															<div class="row">
																<div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
																	<label class="hrzn-fm">Tanggal Register </label>
																</div>
																<div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
																	<div class="nk-int-st">
																		<input type="date" class="form-control input-sm" name="tanggal_regist" placeholder="Tanggal register Barang">
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="form-example-int form-horizental">
														<div class="form-group">
															<div class="row">
																<div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
																	<label class="hrzn-fm">Ruang </label>
																</div>
																<div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
																	<div class="nk-int-st">
																		<select name="id_ruang" required class="select2_group form-control">
																			<option value="">Pilih Ruang</option>
																			<?php
																				include "koneksi.php";
																				$select=mysqli_query($connect,"select * from ruang");
																				while($data=mysqli_fetch_array($select)){
																			?>
																			<option value="<?php echo $data['id_ruang'];?>"><?php echo $data['nama_ruang'];?></option>
																			<?php }?>
																		</select>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="form-example-int form-horizental mg-t-15">
														<div class="form-group">
															<div class="row">
																<div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
																	<label class="hrzn-fm">Kode Inventaris </label>
																</div>
																<div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
																	<div class="nk-int-st">
																		<input type="text" class="form-control input-sm" name="kd_inv" placeholder="Kode Inventaris Barang">
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="form-example-int form-horizental mg-t-15">
														<div class="form-group">
															<div class="row">
																<div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
																	<label class="hrzn-fm"> Petugas </label>
																</div>
																<div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
																	<div class="nk-int-st">
																	<select name="id_petugas" required class="select2_group form-control">
																			<option value="">Pilih Petugas</option>
																			<?php
																				include "koneksi.php";
																				$select=mysqli_query($connect,"select * from petugas");
																				while($data=mysqli_fetch_array($select)){
																			?>
																			<option value="<?php echo $data['id_petugas'];?>"><?php echo $data['nama_petugas'];?></option>
																			<?php }?>
																		</select>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="form-group">
												<input class="btn-primary" type="submit" name="submit" value="Simpan" />
												<input class="btn-danger" type="reset" name="reset" value="Reset" />
											</div>
												</form>
											</div>
                                        </div>
                                    </div>
                                </div>
											</div>
						</div>
                        <div class="table-responsive">
                            <table id="data-table-basic" class="table table-striped">
                                <thead>
                                    <tr>
										<th>Kode Inventaris </th>
                                        <th>Nama</th>
                                        <th>Jumlah</th>
										<th>Petugas</th>
										<th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
				include "koneksi.php";
				$sql=mysqli_query($connect,"select * from inventaris i 
					left join jenis j on i.id_jenis=j.id_jenis
					left join ruang r on i.id_ruang=r.id_ruang
					left join petugas p on i.id_jenis=p.id_petugas ");
				while($hasil=mysqli_fetch_array($sql)){;
				$kode_inventaris= $hasil['kode_inventaris'];
			?>
			<tr>
			<td align="center"><?php echo $hasil["kode_inventaris"]; ?></td>
			<td align="center"><?php echo $hasil["nama"]; ?></td>
			<td align="center"><?php echo $hasil["jumlah"]; ?></td>
			<td align="center"><?php echo $hasil["nama_petugas"]; ?></td>
										<td>
                            <a href="i_edit.php?id_inventaris=<?php echo $hasil['id_inventaris']; ?>"><button class="btn btn-primary notika-btn-primary">edit</button></a>
                            <a href="hapus_db.php?id_inventaris=<?php echo $hasil['id_inventaris']; ?>"><button class="btn btn-danger notika-btn-danger">hapus</button></a>
							<a href="#myModal?id_inventaris=<?php echo $hasil['id_inventaris']; ?>"><button type="button" class="btn notika-btn-teal" data-toggle="modal"
							data-target="#myModal<?php echo $hasil['id_inventaris']; ?>"><font color="white">detail</font></button></a>
							</td></tr>
										<div class="modal animated rubberBand" id="myModal<?php echo $hasil['id_inventaris']; ?>" role="dialog">
                                    <div class="modal-dialog modals-default">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                                
						<div class="table-responsive">
                            <table id="data-table-basic" class="table table-striped">
                <tr>
                    <td>Nama Barang</td>
                    <td>:</td>
                    <td><?php echo $hasil['nama']; ?></td>
                </tr>
                <tr>
                    <td>Kondisi</td>
                    <td>:</td>
                    <td><?php echo $hasil['kondisi']; ?></td>
                </tr>
				<tr>
                    <td>Keterangan</td>
                    <td>:</td>
                    <td><?php echo $hasil['keterangan']; ?></td>
                </tr>
				<tr>
                    <td>Jumlah</td>
                    <td>:</td>
                    <td><?php echo $hasil['jumlah']; ?></td>
                </tr>
				<tr>
                    <td>Nama Jenis</td>
                    <td>:</td>
                    <td><?php echo $hasil['nama_jenis']; ?></td>
                </tr>
				<tr>
                    <td>Tanggal Register</td>
                    <td>:</td>
                    <td><?php echo $hasil['tanggal_register']; ?></td>
                </tr>
				<tr>
                    <td>Nama Ruang</td>
                    <td>:</td>
                    <td><?php echo $hasil['nama_ruang']; ?></td>
                </tr>
				<tr>
                    <td>Kode Inventaris</td>
                    <td>:</td>
                    <td><?php echo $hasil['kode_inventaris']; ?></td>
                </tr>
				<tr>
                    <td>Nama Petugas</td>
                    <td>:</td>
                    <td><?php echo $hasil['nama_petugas']; ?></td>
                </tr>
				
            </table>
											</div>
                                        </div>
                                    </div>
                                </div>
								</div>
			<?php
              };
            ?>
                                </tbody>
							</table>
						

    <!-- End Footer area-->
    <!-- jquery
		============================================ -->
    <script src="tampilan/js/vendor/jquery-1.12.4.min.js"></script>
    <!-- bootstrap JS
		============================================ -->
    <script src="tampilan/js/bootstrap.min.js"></script>
    <!-- wow JS
		============================================ -->
    <script src="tampilan/js/wow.min.js"></script>
    <!-- price-slider JS
		============================================ -->
    <script src="tampilan/js/jquery-price-slider.js"></script>
    <!-- owl.carousel JS
		============================================ -->
    <script src="tampilan/js/owl.carousel.min.js"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="tampilan/js/jquery.scrollUp.min.js"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="tampilan/js/meanmenu/jquery.meanmenu.js"></script>
    <!-- counterup JS
		============================================ -->
    <script src="tampilan/js/counterup/jquery.counterup.min.js"></script>
    <script src="tampilan/js/counterup/waypoints.min.js"></script>
    <script src="tampilan/js/counterup/counterup-active.js"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="tampilan/js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- sparkline JS
		============================================ -->
    <script src="tampilan/js/sparkline/jquery.sparkline.min.js"></script>
    <script src="tampilan/js/sparkline/sparkline-active.js"></script>
    <!-- flot JS
		============================================ -->
    <script src="tampilan/js/flot/jquery.flot.js"></script>
    <script src="tampilan/js/flot/jquery.flot.resize.js"></script>
    <script src="tampilan/js/flot/flot-active.js"></script>
    <!-- knob JS
		============================================ -->
    <script src="tampilan/js/knob/jquery.knob.js"></script>
    <script src="tampilan/js/knob/jquery.appear.js"></script>
    <script src="tampilan/js/knob/knob-active.js"></script>
    <!--  Chat JS
		============================================ -->
    <script src="tampilan/js/chat/jquery.chat.js"></script>
    <!--  todo JS
		============================================ -->
    <script src="tampilan/js/todo/jquery.todo.js"></script>
	<!--  wave JS
		============================================ -->
    <script src="tampilan/js/wave/waves.min.js"></script>
    <script src="tampilan/js/wave/wave-active.js"></script>
    <!-- plugins JS
		============================================ -->
    <script src="tampilan/js/plugins.js"></script>
    <!-- Data Table JS
		============================================ -->
    <script src="tampilan/js/data-table/jquery.dataTables.min.js"></script>
    <script src="tampilan/js/data-table/data-table-act.js"></script>
    <!-- main JS
		============================================ -->
    <script src="tampilan/js/main.js"></script>
	<!-- tawk chat JS
		============================================ -->
    <script src="tampilan/js/tawk-chat.js"></script>
</body>

</html>