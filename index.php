<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->

<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>ujikom inventaris</title>
    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8" />
    <meta name="keywords" content="Toon Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script>
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }

    </script>
    <!-- //Meta tag Keywords -->

    <!-- Custom-Files -->
    <link rel="stylesheet" href="tampilan_awal/css/bootstrap.css">
    <!-- Bootstrap-Core-CSS -->
    <link rel="stylesheet" href="tampilan_awal/css/style.css" type="text/css" media="all" />
    <!-- Style-CSS -->
    <!-- font-awesome-icons -->
    <link href="tampilan_awal/css/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome-icons -->
    <!-- /Fonts -->
    <link href="//fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900" rel="stylesheet">
    <!-- //Fonts -->

</head>

<body>
    <!-- mian-content -->
    <section class="main-content" id="home">

        <!-- header -->
        <header>
            <div class="container-fluid px-lg-5">
                <!-- nav -->
                <nav>
                    <div class="logo-w3ls" id="logo-w3ls">
                        <h1>
                            <a href="index.php"><span>inventaris</span></a>
                        </h1>

                    </div>

                   <ul class="menu">
                        <li class="mr-lg-4 mr-3 active"><a href="index.php">INVENTARIS SEKOLAH</a></li>
                        <li class="mr-lg-4 mr-3"><a href="hal_login.php">LOGIN</a></li>
                         
                    </ul>
                </nav>
                <!-- //nav -->
            </div>
        </header>
        <!-- //header -->
                <h3 class="tittle text-center"><span class="sub-tittle"></span></h3>
				<img width="100%" height="100%" src="tampilan_awal/images/banner4.jpg">
		
			</div>
        </div>
    </section>
		

        <!-- header -->
        <section class="about py-lg-5 py-md-5 py-3">
        <div class="container">
                <h3 class="tittle text-center"><span class="sub-tittle"></span>Inventaris Barang</h3>
                <div class="card-deck mt-5">
                    <div class="card">
                            <table border="0" bgcolor="#87CEFA" width="100%" height="100%" align="center">
							<tr>
							<td valign="justify">
<h2>A.      PENGERTIAN</h2><br>
Yang  dimaksud  dengan  inventarisasi  adalah  kegiatan  melaksanakan  pengurusan, penyelengaraan, pengaturan, pencatatan dan pendaftaran barang inventaris. 
<br>Daftar barang inventaris adalah suatu dokumen berharga yang menunjukkan sejumlah barang milik lembaga dan dikuasai pimpinan sekolah , 
baik yang bergerak maupun yang tidak bergerak. Adanya daftar inventaris yang lengkap,
<br> teratur dan berkelanjutan di semua tingkat sekolah mempunyaifungsi dan peranan yang sangat penting  dalam rangka :
<br>1.        Tertib administrasi dan tertib barang milik lembaga (Yayasan).
<br>2.        Pendaftaran, pengendalian dan pengawasan setiap hak milik lembaga (Yayasan).
<br>3.  Usaha untuk memanfaatkan penggunaan setiap barang lembaga secara maksimal dalam melancarkan pencapaian maksud dan tujuan.
<br>4.   Menunjang  pelaksanaan Penyelenggaraan Pimpinan unit unit karya melancarkan pencapaian maksud dan tujuan .
<br>
<h2>B.		Manfaat Utama Sistem Manajemen Inventory</h2>
<br>
Sistem manajemen inventory dapat menyederhanakan proses pengelolaan persediaan yang kompleks, seperti pengecekan inventaris, pemesanan barang dari pemasok, 
<br>pengambilan dan pengepakan barang, penghitungan nilai inventaris, prediksi kebutuhan inventaris, pelacakan pengiriman pesanan, dan masih banyak lagi.<br>

Berikut ini adalah beberapa manfaat utama sistem manajemen inventory yang juga menjadi alasan bagi banyak sekolah untuk beralih menggunakan solusi otomatis ini:
<br>1.)Penyimpanan Data Terpusat
<br>2.)Stock Control
<br>3.)Peningkatan Efisiensi dan Produktivitas yang Optimal
<br>4.)Penghematan Biaya
<br>5.)Prakiraan & Perencanaan yang Akurat
							</td>
							</tr>
							</table>
							</div>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
 
</body>

</html>
