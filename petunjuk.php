<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->

<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>ujikom inventaris</title>
    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8" />
    <meta name="keywords" content="Toon Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script>
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }

    </script>
    <!-- //Meta tag Keywords -->

    <!-- Custom-Files -->
    <link rel="stylesheet" href="tampilan_awal/css/bootstrap.css">
    <!-- Bootstrap-Core-CSS -->
    <link rel="stylesheet" href="tampilan_awal/css/style.css" type="text/css" media="all" />
    <!-- Style-CSS -->
    <!-- font-awesome-icons -->
    <link href="tampilan_awal/css/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome-icons -->
    <!-- /Fonts -->
    <link href="//fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900" rel="stylesheet">
    <!-- //Fonts -->

</head>

<body>
    <!-- mian-content -->
    <section class="main-content" id="home">

        <!-- header -->
        <header>
            <div class="container-fluid px-lg-5">
                <!-- nav -->
                <nav>
                    <div class="logo-w3ls" id="logo-w3ls">
                        <h1>
                            <a href="index.php"><span>inventaris</span></a>
                        </h1>

                    </div>

                   <ul class="menu">
                        <li class="mr-lg-4 mr-3"><a href="index.php">INVENTARIS SEKOLAH</a></li>
                        <li class="mr-lg-4 mr-3 active"><a href="petunjuk.php">PETUNJUK PENGGUNAAN</a></li>
                        <li class="mr-lg-4 mr-3">
                            <!-- First Tier Drop Down -->
                            <label for="drop-2" class="toggle">Dropdown <span class="fa fa-sort-desc" aria-hidden="true"></span> </label>
                            <a href="#">AKUN <span class="fa fa-sort-desc" aria-hidden="true"></span></a>
                            <input type="checkbox" id="drop-2" />
                            <ul>
                                <li><a href="hal_login.php">LOGIN</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
                <!-- //nav -->
            </div>
        </header>
        <!-- //header -->


        <!-- //header -->
                <h3 class="tittle text-center"><span class="sub-tittle"></span></h3>
				<img width="100%" height="100%" src="tampilan_awal/images/banner4.jpg">
		
			</div>
        </div>
    </section>    <!--// banner-slider -->
 <!-- services  -->
    <section class="services-top bg-light py-5">
        <div class="container">
            <div class="inner-sec-w3layouts py-lg-5 py-3">
                <h3 class="tittle text-center"><span class="sub-tittle"></span>Petunjuk Penggunaan</h3>
                <!-- services top row -->
                <div class="row pt-md-5">
                    <div class="col-md-6 services-gd-img">
                        <img src="tampilan_awal/images/ab4.jpg" class="img-fluid" alt="user-image">
                    </div>
                    <div class="col-md-6 services-gd-info">
                        <div class="my-3 services-gd">
                            <div class="card-body">
                                <span class="fa fa-bell-o" aria-hidden="true"></span>
                                <h5 class="card-title">Card title here</h5>
                                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                        </div>
                        <div class="services-gd my-2">
                            <div class="card-body">
                                <span class="fa fa-binoculars" aria-hidden="true"></span>
                                <h5 class="card-title">Card title here</h5>
                                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                        </div>
                        <div class="my-3 services-gd">
                            <div class="card-body">
                                <span class="fa fa-flask" aria-hidden="true"></span>
                                <h5 class="card-title">Card title here</h5>
                                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </div>
    </section>
    <!-- services -->

    
</body>

</html>
