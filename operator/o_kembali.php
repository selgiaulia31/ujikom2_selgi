<?php
include "../koneksi.php";
$id=$_GET['id_peminjaman'];

$select=mysqli_query($connect,"select * from peminjaman where id_peminjaman='$id'");
$data=mysqli_fetch_assoc($select);
?>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>UJIKOM INVENTARIS</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="../tampilan/img/favicon.ico">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="../tampilan/css/bootstrap.min.css">
    <!-- font awesome CSS
		============================================ -->
    <link rel="stylesheet" href="../tampilan/css/font-awesome.min.css">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="../tampilan/css/owl.carousel.css">
    <link rel="stylesheet" href="../tampilan/css/owl.theme.css">
    <link rel="stylesheet" href="../tampilan/css/owl.transitions.css">
    <!-- meanmenu CSS
		============================================ -->
    <link rel="stylesheet" href="../tampilan/css/meanmenu/meanmenu.min.css">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="../tampilan/css/animate.css">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="../tampilan/css/normalize.css">
	<!-- wave CSS
		============================================ -->
    <link rel="stylesheet" href="../tampilan/css/wave/waves.min.css">
    <link rel="stylesheet" href="../tampilan/css/wave/button.css">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="../tampilan/css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- Notika icon CSS
		============================================ -->
    <link rel="stylesheet" href="../tampilan/css/notika-custom-icon.css">
    <!-- Data Table JS
		============================================ -->
    <link rel="stylesheet" href="../tampilan/css/jquery.dataTables.min.css">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="../tampilan/css/main.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="../tampilan/style.css">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="../tampilan/css/responsive.css">
    <!-- modernizr JS
		============================================ -->
    <script src="../tampilan/js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- Start Header Top Area -->
    <div class="header-top-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="logo-area">
                        <h2><font color="white"> <i class="notika-icon notika-app"></i> Inventarisir </font></h2>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="header-top-menu">
                        <ul class="nav navbar-nav notika-top-nav">
                            <li class="nav-item dropdown">
                                <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><span></span></a>
                                <div role="menu" class="dropdown-menu search-dd animated flipInX">
                                    <div class="search-input">
                                        <i class="notika-icon notika-left-arrow"></i>
                                        <input type="text" />
                                    </div>
                                </div>
                            </li>
						</ul>
                    </div>
            </div>
        </div>
    </div>
    <!-- End Header Top Area -->
    <!-- Mobile Menu start -->
    <div class="mobile-menu-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="mobile-menu">
                        <nav id="dropdown">
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Mobile Menu end -->
    <!-- Main Menu area start-->
    <div class="main-menu-area mg-tb-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul class="nav nav-tabs notika-menu-wrap menu-it-icon-pro">
                        <li><a href="o_inventarisir.php"><i class="notika-icon notika-house"></i> Inventarisir </a>
                        </li>
                        <li class="active"><a href="o_peminjaman.php"><i class="notika-icon notika-mail"></i> Peminjaman</a>
                        </li>
                         <li><a href="o_logout.php"><i class="notika-icon notika-form"></i> logout</a>
                        </li>
                    </ul>
                    <div class="tab-content custom-menu-content">
                        
                        <div id="UA" class="tab-pane notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="o_pengaturan_user.php">Pengaturan Akun</a>
                                </li>
                                <li><a href="logout.php">Logout</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Main Menu area End-->
	<!-- Breadcomb area Start-->
	
	<!-- Breadcomb area End-->
    <!-- Data Table area Start-->
    <div class="data-table-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="data-table-list"> 
                        <div class="basic-tb-hd">
                            <center><h2>Data Barang yang akan dikembalikan</h2><br></center>
<div class="col-lg-8 col-md-8 col-sm-6">
<div class="modals-default-cl">
											<div class="modal animated rubberBand" id="myModalsix" role="dialog">
                                    <div class="modal-dialog modals-default">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            </div>
                                    </div>
                                </div>
							</div>
						</div>
					<?php
						$select=mysqli_query($connect,"SELECT * FROM peminjaman pe left join detail_pinjam dp on dp.kd_pinjam=pe.kd_pinjam 
																				   join inventaris on dp.id_inventaris = inventaris.id_inventaris
																				   join pegawai on pe.id_pegawai=pegawai.id_pegawai
																    WHERE id_peminjaman = $id");

									$data=mysqli_fetch_array($select);
					?>
											<form action="proses_kembali.php" method="post" enctype="form-horizontal form-label-left" >
											<div class="form-group">			
												<input value="<?php echo $data['id_peminjaman'];?>" type="hidden" class="form-control" name="id_peminjaman" readonly>		
											</div>
											<div class="form-group">			
												<input value="<?php echo $data['id_inventaris'];?>" type="hidden" class="form-control" name="id_inventaris" readonly>		
											</div>
											<div class="form-group">			
												<input value="<?php echo $data['kd_pinjam'];?>" type="hidden" class="form-control" name="id_detail_pinjam" readonly>		
											</div>
											<div class="form-group">			
												<label> kode inventaris </label>
												<input value="<?php echo $data['kode_inventaris'];?>" type="text" class="form-control" name="kd_inventaris"readonly>		
											</div>
											<div class="form-group">			
												<label> nama barang </label>
												<input value="<?php echo $data['nama'];?>" type="text" class="form-control" name="nama"readonly>		
											</div>
											<div class="form-group">			
												<label> jumlah pinjam </label>
												<input value="<?php echo $data['jumlah_pinjam'];?>" type="text" class="form-control" name="jml"readonly>		
											</div>
											<div class="form-group">			
												<label> Tanggal pinjam </label>
												<input value="<?php echo $data['tanggal_pinjam'];?>" type="text" class="form-control" name="tgl_pinjam"readonly>		
											</div>
											
											<div class="form-group">			
												<label> Nama pegawai </label>
												<input value="<?php echo $data['nama_pegawai'];?>" type="text" class="form-control" name="keterangan"readonly>		
											</div>
											<div class="form-group">			
												<label> keterangan </label>
												<input value="Barang ini akan dikembalikan" type="text" class="form-control" name="keterangan"readonly>		
											</div>
											
											<div class="form-group">
												<input class="btn-primary" type="submit" name="submit" value="Kembalikan Barang" />
											</div>
										</form>

										</div>
									</div>
								</div>
							</div>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <div class="footer-copyright-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="footer-copy-right">
                       
                    </div>
                </div>
            </div>
        </div>
    </div></body>

</html>