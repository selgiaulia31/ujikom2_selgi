<?php
session_start();
if(!isset($_SESSION['username'])){
	die("<script>alert('silahkan login');
	document.location.href='hal_login.php' 
	</script>");
}
?>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>UJIKOM INVENTARIS</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="../tampilan/img/favicon.ico">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="../tampilan/css/bootstrap.min.css">
    <!-- font awesome CSS
		============================================ -->
    <link rel="stylesheet" href="../tampilan/css/font-awesome.min.css">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="../tampilan/css/owl.carousel.css">
    <link rel="stylesheet" href="../tampilan/css/owl.theme.css">
    <link rel="stylesheet" href="../tampilan/css/owl.transitions.css">
    <!-- meanmenu CSS
		============================================ -->
    <link rel="stylesheet" href="../tampilan/css/meanmenu/meanmenu.min.css">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="../tampilan/css/animate.css">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="../tampilan/css/normalize.css">
	<!-- wave CSS
		============================================ -->
    <link rel="stylesheet" href="../tampilan/css/wave/waves.min.css">
    <link rel="stylesheet" href="../tampilan/css/wave/button.css">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="../tampilan/css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- Notika icon CSS
		============================================ -->
    <link rel="stylesheet" href="../tampilan/css/notika-custom-icon.css">
    <!-- Data Table JS
		============================================ -->
    <link rel="stylesheet" href="../tampilan/css/jquery.dataTables.min.css">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="../tampilan/css/main.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="../tampilan/style.css">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="../tampilan/css/responsive.css">
    <!-- modernizr JS
		============================================ -->
    <script src="../tampilan/js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- Start Header Top Area -->
    <div class="header-top-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="logo-area">
                        <h2><font color="white"> <i class="notika-icon notika-app"></i> Inventarisir </font></h2>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="header-top-menu">
                        <ul class="nav navbar-nav notika-top-nav">
                            <li class="nav-item dropdown">
                                <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><span></span></a>
                                <div role="menu" class="dropdown-menu search-dd animated flipInX">
                                    <div class="search-input">
                                        <i class="notika-icon notika-left-arrow"></i>
                                        <input type="text" />
                                    </div>
                                </div>
                            </li>
						</ul>
                    </div>
            </div>
        </div>
    </div>
    <!-- End Header Top Area -->
    <!-- Mobile Menu start -->
    <div class="mobile-menu-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="mobile-menu">
                        <nav id="dropdown">
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Mobile Menu end -->
    <!-- Main Menu area start-->
    <div class="main-menu-area mg-tb-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul class="nav nav-tabs notika-menu-wrap menu-it-icon-pro">
                        <li><a href="o_inventarisir.php"><i class="notika-icon notika-house"></i> Inventarisir </a>
                        </li>
                        <li class="active"><a href="o_peminjaman.php"><i class="notika-icon notika-mail"></i> Peminjaman</a>
                        </li>
                         <li><a href="o_logout.php"><i class="notika-icon notika-form"></i> logout</a>
                        </li>
                    </ul>
                    <div class="tab-content custom-menu-content">
                        
                        <div id="UA" class="tab-pane notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="o_pengaturan_user.php">Pengaturan Akun</a>
                                </li>
                                <li><a href="logout.php">Logout</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Main Menu area End-->
	<!-- Breadcomb area Start-->
	
	<!-- Breadcomb area End-->
    <!-- Data Table area Start-->
   <!-- Data Table area Start-->
    <div class="data-table-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="widget-tabs-int mg-t-30">
                        <div class="tab-hd">
							</div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="widget-tabs-list tab-pt-mg">
                                    <ul class="nav nav-tabs tab-nav-center">
                                        <li class="active"><a data-toggle="tab" href="#home3">Data Barang Dipinjam</a></li>	
                                    </ul>
                                    <div class="tab-content tab-custom-st">
                                        <div id="home3" class="tab-pane in active animated zoomInLeft">
                                            <div class="tab-ctn">
                                            <div class="basic-tb-hd">
                            <h2>Data Barang Dipinjam </h2>
											
						</div>
                        <div class="table-responsive">
						    <table id="data-table-basic" class="table table-striped">
                                <thead>
                                    <tr>
										<th>Kode Barang</th>
                                        <th>Nama Barang</th>
										<th>Jumlah Barang Tersedia</th>
										<th>Jumlah Dipinjam</th>
										<th> Aksi </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
				include "../koneksi.php";
				$sql=mysqli_query($connect,"select * from peminjaman pe left join detail_pinjam dp on dp.kd_pinjam=pe.kd_pinjam
					join inventaris on dp.id_inventaris = inventaris.id_inventaris
					left join pegawai g on pe.id_pegawai=g.id_pegawai
					left join petugas z on z.nama_petugas=g.nama_pegawai
					where status_peminjaman='dipinjam' AND  username='".$_SESSION['username']."'");
									
				while($hasil=mysqli_fetch_array($sql)){;
			?>
			<tr>
			<form action="o_kembali.php?id_peminjaman=<?php echo $id_peminjaman; ?>" method="post" enctype="form-horizontal form-label-left" >
						
                <div class="form-group" >			
					<input value="<?php echo $hasil['id_inventaris'];?>" type="hidden" class="form-control" name="id_inventaris" readonly>		
				</div>
            
			<td>
                <div class="form-group">			
					<input value="<?php echo $hasil['kode_inventaris'];?>" type="text" class="form-control" name="kode_inventaris" readonly>		
				</div>
            </td><td>
                <div class="form-group">			
					<input value="<?php echo $hasil['nama'];?>" type="text" class="form-control" name="nama" readonly>		
				</div>
            </td>
            <td>
                <div class="form-group">			
					<input value="<?php echo $hasil['jumlah'];?>" type="text" class="form-control" name="jumlah" readonly>		
				</div>
            </td>
			<td>
                <div class="form-group">			
					<input value="<?php echo $hasil['jumlah_pinjam'];?>" type="text" class="form-control" name="jml" readonly>		
				</div>
      </td></form>
			<td>
			<a href="o_kembali.php?id_peminjaman=<?php echo $hasil['id_peminjaman']; ?>"><button class="btn btn-primary notika-btn-primary">Kembalikan Barang</button></a>
			</td>
            </tr>
									<?php
              } $connect->close(); 
            ?>
                                </tbody>
							</table>
						</div>   
											</div>
                                        </div>
										
										
                                       
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

	<div class="modal animated rubberBand" id="#TDB" role="dialog">
                                    <div class="modal-dialog modals-default">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                                <h2>Modal title</h2>
                                                <p>Curabitur blandit mollis lacus. Nulla sit amet est. Suspendisse nisl elit, rhoncus eget, elementum ac, condimentum eget, diam. Donec mi odio, faucibus at, scelerisque quis, convallis in, nisi. Cras sagittis.</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Save changes</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
    <div class="footer-copyright-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="footer-copy-right">
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- End Footer area-->
    <!-- jquery
		============================================ -->
    <script src="tampilan/js/vendor/jquery-1.12.4.min.js"></script>
    <!-- bootstrap JS
		============================================ -->
    <script src="tampilan/js/bootstrap.min.js"></script>
    <!-- wow JS
		============================================ -->
    <script src="tampilan/js/wow.min.js"></script>
    <!-- price-slider JS
		============================================ -->
    <script src="tampilan/js/jquery-price-slider.js"></script>
    <!-- owl.carousel JS
		============================================ -->
    <script src="tampilan/js/owl.carousel.min.js"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="tampilan/js/jquery.scrollUp.min.js"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="tampilan/js/meanmenu/jquery.meanmenu.js"></script>
    <!-- counterup JS
		============================================ -->
    <script src="tampilan/js/counterup/jquery.counterup.min.js"></script>
    <script src="tampilan/js/counterup/waypoints.min.js"></script>
    <script src="tampilan/js/counterup/counterup-active.js"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="tampilan/js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- sparkline JS
		============================================ -->
    <script src="tampilan/js/sparkline/jquery.sparkline.min.js"></script>
    <script src="tampilan/js/sparkline/sparkline-active.js"></script>
    <!-- flot JS
		============================================ -->
    <script src="tampilan/js/flot/jquery.flot.js"></script>
    <script src="tampilan/js/flot/jquery.flot.resize.js"></script>
    <script src="tampilan/js/flot/flot-active.js"></script>
    <!-- knob JS
		============================================ -->
    <script src="tampilan/js/knob/jquery.knob.js"></script>
    <script src="tampilan/js/knob/jquery.appear.js"></script>
    <script src="tampilan/js/knob/knob-active.js"></script>
    <!--  Chat JS
		============================================ -->
    <script src="tampilan/js/chat/jquery.chat.js"></script>
    <!--  todo JS
		============================================ -->
    <script src="tampilan/js/todo/jquery.todo.js"></script>
	<!--  wave JS
		============================================ -->
    <script src="tampilan/js/wave/waves.min.js"></script>
    <script src="tampilan/js/wave/wave-active.js"></script>
    <!-- plugins JS
		============================================ -->
    <script src="tampilan/js/plugins.js"></script>
    <!-- Data Table JS
		============================================ -->
    <script src="tampilan/js/data-table/jquery.dataTables.min.js"></script>
    <script src="tampilan/js/data-table/data-table-act.js"></script>
    <!-- main JS
		============================================ -->
    <script src="tampilan/js/main.js"></script>
	<!-- tawk chat JS
		============================================ -->
    <script src="tampilan/js/tawk-chat.js"></script>
</body>

</html>