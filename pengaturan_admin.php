<?php
session_start();
if(!isset($_SESSION['username'])){
	die("<script>alert('silahkan login');
	document.location.href='hal_login.php' 
	</script>");
}
?>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>UJIKOM INVENTARIS</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="tampilan/img/favicon.ico">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/bootstrap.min.css">
    <!-- font awesome CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/font-awesome.min.css">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/owl.carousel.css">
    <link rel="stylesheet" href="tampilan/css/owl.theme.css">
    <link rel="stylesheet" href="tampilan/css/owl.transitions.css">
    <!-- meanmenu CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/meanmenu/meanmenu.min.css">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/animate.css">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/normalize.css">
	<!-- wave CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/wave/waves.min.css">
    <link rel="stylesheet" href="tampilan/css/wave/button.css">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- Notika icon CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/notika-custom-icon.css">
    <!-- Data Table JS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/jquery.dataTables.min.css">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/main.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/style.css">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/responsive.css">
    <!-- modernizr JS
		============================================ -->
    <script src="tampilan/js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- Start Header Top Area -->
    <div class="header-top-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="logo-area">
                        <h2><font color="white"> <i class="notika-icon notika-app"></i> Inventarisir </font></h2>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="header-top-menu">
                        <ul class="nav navbar-nav notika-top-nav">
                            <li class="nav-item dropdown">
                                <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><span></span></a>
                                <div role="menu" class="dropdown-menu search-dd animated flipInX">
                                    <div class="search-input">
                                        <i class="notika-icon notika-left-arrow"></i>
                                        <input type="text" />
                                    </div>
                                </div>
                            </li>
						</ul>
                    </div>
            </div>
        </div>
    </div>
    <!-- End Header Top Area -->
    <!-- Mobile Menu start -->
    <div class="mobile-menu-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="mobile-menu">
                        <nav id="dropdown">
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Mobile Menu end -->
    <!-- Main Menu area start-->
    <div class="main-menu-area mg-tb-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul class="nav nav-tabs notika-menu-wrap menu-it-icon-pro">
                        <li ><a href="inventarisir.php"><i class="notika-icon notika-house"></i> Inventarisir </a>
                        </li>
                        <li ><a href="peminjaman.php"><i class="notika-icon notika-mail"></i> Peminjaman</a>
                        </li>
                        <li ><a href="pengembalian.php"><i class="notika-icon notika-edit"></i> Pengembalian</a>
                        </li>
                        <li><a href="laporan.php"><i class="notika-icon notika-bar-chart"></i> Laporan Data </a>
                        </li>
                        <li ><a data-toggle="tab" href="#Referensi"><i class="notika-icon notika-windows"></i> Referensi</a>
                        </li>
                        <li><a data-toggle="tab" href="#UA"><i class="notika-icon notika-form"></i> User Account</a>
                        </li>
                    </ul>
                    <div class="tab-content custom-menu-content">
                                               <div id="DB" class="tab-pane in notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="inventarisir.php">Data Peminjaman</a>
                                </li>
                                </li>
                            </ul>
                        </div>
						<div id="PB" class="tab-pane in notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="peminjaman.php">Peminjaman</a>
                                </li>
                                </li>
                            </ul>
                        </div>
						<div id="KB" class="tab-pane in notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="pengembalian.php">Pengembalian</a>
                                </li>
                                </li>
                            </ul>
                        </div>
						<div id="Laporan" class="tab-pane in notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="inventarisir.php">Data Barang</a>
                                </li>
                                <li><a href="laporan_peminjaman.html">Data Peminjaman</a>
                                </li>
                                <li><a href="ua.html">User Akun</a>
                                </li>
                                </li>
                            </ul>
                        </div>
                        <div id="Referensi" class="tab-pane notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li class="active"><a href="jenis.php">Jenis</a></li>
								<li><a href="level.php">Level</a></li>
								<li><a href="ruang.php">Ruang</a></li>
								<li><a href="pegawai.php">Pegawai</a></li>
								<li><a href="petugas.php">Petugas</a></li>
                            </ul>
                        </div>
                        <div id="UA" class="tab-pane notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="pengaturan_admin.php">Pengaturan Akun</a>
                                </li>
                                <li><a href="logout.php">Logout</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Main Menu area End-->
	<!-- Breadcomb area Start-->
	
	<!-- Breadcomb area End-->
    <!-- Data Table area Start-->
    <div class="data-table-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="data-table-list"> 
                        <div class="basic-tb-hd">
                            <h2>Data Akun User </h2>
							<table >
									<?php
										include "koneksi.php";
										$select=mysqli_query($connect,"select * from petugas p left join level g on p.id_level=g.id_level
																							where username='".$_SESSION['username']."'");
										while($data=mysqli_fetch_array($select)){
									?>
										
									<tr>
										<td rowspan="10"><img src="../img/3.png" width="200"> </td>
										<td rowspan="10"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
										<td>Username</td>
										<td> : </td>
										<td rowspan="10"> &nbsp;&nbsp; </td>
										<td><?php echo $data['username'] ?></td>
									</tr>
									<tr>
										<td>NIP</td>
										<td> : </td>
										<td><?php echo $data['nip'] ?></td>
									</tr>
									<tr>
										<td>Nama </td>
										<td> : </td>
										<td><?php echo $data['nama_pegawai'] ?></td>
									</tr>
									<tr>
										<td>Alamat</td>
										<td> : </td>
										<td><?php echo $data['alamat'] ?></td>
									</tr>
									<tr>
										<td>Jenis Kelamin</td>
										<td> : </td>
										<td><?php echo $data['jk'] ?></td>
									</tr>
									<tr>
										<td>Level</td>
										<td> : </td>
										<td><?php echo $data['nama_level'] ?></td>
									</tr>
									<?php }?>
									
									</table>
									
						</div>
					</div>	
				</div>		
			</div>			
		</div>					
	</div>
<div class="footer-copyright-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="footer-copy-right">
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Footer area-->
</body>

</html>