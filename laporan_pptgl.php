<?php
include 'koneksi.php';
require('assets/fpdf.php');

$pdf = new FPDF("L","cm","A4");

$pdf->SetMargins(2,1,1);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',11);
$pdf->SetX(4);            
$pdf->MultiCell(19.5,0.5,'Inventaris Sekolah',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Telpon : 0251xxxxxxxx',0,'L');    
$pdf->SetFont('Arial','B',10);
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'JL. Laladon ',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'website : www.smkn1ciomas.com email : smkn1ciomas@gmail.com',0,'L');
$pdf->Line(1,3.1,28.5,3.1);
$pdf->SetLineWidth(0.1);      
$pdf->Line(1,3.2,28.5,3.2);   
$pdf->SetLineWidth(0);
$pdf->ln(1);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(25.5,0.7,"Laporan Data peminjaman",0,10,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(5,0.7,"Di cetak pada : ".date("D-d/m/Y"),0,0,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(1, 0.8, 'NO', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'kode pinjam', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'tanggal pinjam', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'tanggal kembali', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'kode inventaris', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'nama barang', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'jumlah pinjam', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'status pinjam', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Nama Pegawai', 1, 1, 'C');


$pdf->SetFont('Arial','',10);
$no=1;
$sql=mysqli_query($connect,"select * from peminjaman pe 
				left join pegawai on pe.id_pegawai=pegawai.id_pegawai
				left join detail_pinjam dp on dp.kd_pinjam=pe.kd_pinjam 
				join inventaris on dp.id_inventaris = inventaris.id_inventaris where pe.tanggal_pinjam BETWEEN '$_GET[tgl1]' and '$_GET[tgl2]'");
				while($lihat=mysqli_fetch_array($sql)){;
	$pdf->Cell(1, 0.8, $no , 1, 0, 'C');
	$pdf->Cell(3, 0.8, $lihat['kode'],1, 0, 'C');
	$pdf->Cell(3, 0.8, $lihat['tanggal_pinjam'], 1, 0,'C');
	$pdf->Cell(3, 0.8, $lihat['tanggal_kembali'],1, 0, 'C');
	$pdf->Cell(3, 0.8, $lihat['kode_inventaris'], 1, 0,'C');
	$pdf->Cell(3, 0.8, $lihat['nama'], 1, 0,'C');
	$pdf->Cell(3, 0.8, $lihat['jumlah_pinjam'], 1, 0,'C');
	$pdf->Cell(3, 0.8, $lihat['status_peminjaman'], 1, 0,'C');
	$pdf->Cell(3, 0.8, $lihat['nama_pegawai'], 1, 1,'C');
	


	$no++;
}

$pdf->Output("laporan_peminjaman.pdf","I");

?>

