<?php
session_start();
if(!isset($_SESSION['username'])){
	die("<script>alert('silahkan login');
	document.location.href='hal_login.php' 
	</script>");
}
?>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>UJIKOM INVENTARIS</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="tampilan/img/favicon.ico">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/bootstrap.min.css">
    <!-- font awesome CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/font-awesome.min.css">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/owl.carousel.css">
    <link rel="stylesheet" href="tampilan/css/owl.theme.css">
    <link rel="stylesheet" href="tampilan/css/owl.transitions.css">
    <!-- meanmenu CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/meanmenu/meanmenu.min.css">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/animate.css">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/normalize.css">
	<!-- wave CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/wave/waves.min.css">
    <link rel="stylesheet" href="tampilan/css/wave/button.css">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- Notika icon CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/notika-custom-icon.css">
    <!-- Data Table JS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/jquery.dataTables.min.css">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/main.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/style.css">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/responsive.css">
    <!-- modernizr JS
		============================================ -->
    <script src="tampilan/js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- Start Header Top Area -->
    <div class="header-top-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="logo-area">
                        <h2><font color="white"> <i class="notika-icon notika-app"></i> Inventarisir </font></h2>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="header-top-menu">
                        <ul class="nav navbar-nav notika-top-nav">
                            <li class="nav-item dropdown">
                                <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><span></span></a>
                                <div role="menu" class="dropdown-menu search-dd animated flipInX">
                                    <div class="search-input">
                                        <i class="notika-icon notika-left-arrow"></i>
                                        <input type="text" />
                                    </div>
                                </div>
                            </li>
						</ul>
                    </div>
            </div>
        </div>
    </div>
    <!-- End Header Top Area -->
    <!-- Mobile Menu start -->
    <div class="mobile-menu-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="mobile-menu">
                        <nav id="dropdown">
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Mobile Menu end -->
    <!-- Main Menu area start-->
    <div class="main-menu-area mg-tb-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul class="nav nav-tabs notika-menu-wrap menu-it-icon-pro">
                        <li class="active"><a href="u_inventarisir.php"><i class="notika-icon notika-house"></i> Inventarisir </a>
                        </li>
                        <li ><a href="u_peminjaman.php"><i class="notika-icon notika-mail"></i> Peminjaman</a>
                        </li>
                         <li><a href="logout.php"><i class="notika-icon notika-form"></i>Logout</a>
                        </li>
                    </ul>
                    <div class="tab-content custom-menu-content">
                        
                        <div id="UA" class="tab-pane notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="pengaturan_user.php">Pengaturan Akun</a>
                                </li>
                                <li><a href="logout.php">Logout</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Main Menu area End-->
	<!-- Breadcomb area Start-->
	
	<!-- Breadcomb area End-->
    <!-- Data Table area Start-->
    <div class="data-table-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="data-table-list"> 
                        <div class="basic-tb-hd">
                            <h2>Data Barang </h2>
								
                        <div class="table-responsive">
                            <table id="data-table-basic" class="table table-striped">
                                <thead>
                                    <tr>
										<th>Kode Inventaris </th>
                                        <th>Nama</th>
                                        <th>Jumlah</th>
										<th>Petugas</th>
										<th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
				include "koneksi.php";
				$sql=mysqli_query($connect,"select * from inventaris i 
					left join jenis j on i.id_jenis=j.id_jenis
					left join ruang r on i.id_ruang=r.id_ruang
					left join petugas p on i.id_jenis=p.id_petugas ");
				while($hasil=mysqli_fetch_array($sql)){;
				$kode_inventaris= $hasil['kode_inventaris'];
			?>
			<tr>
			<td align="center"><?php echo $hasil["kode_inventaris"]; ?></td>
			<td align="center"><?php echo $hasil["nama"]; ?></td>
			<td align="center"><?php echo $hasil["jumlah"]; ?></td>
			<td align="center"><?php echo $hasil["nama_petugas"]; ?></td>
										<td>
                            <a href="#myModal?id_inventaris=<?php echo $hasil['id_inventaris']; ?>"><button type="button" class="btn notika-btn-teal" data-toggle="modal"
							data-target="#myModal<?php echo $hasil['id_inventaris']; ?>"><font color="white">detail</font></button></a>
							<a href="pinjam_barang.php?id_inventaris=<?php echo $hasil['id_inventaris']; ?>"><button class="btn btn-primary notika-btn-primary">Pinjam Barang</button></a>
                            </td></tr>
										
			<?php
              };
            ?>
                                </tbody>
							</table>
							
							
						                         <?php
				include "koneksi.php";
				$sql=mysqli_query($connect,"select * from inventaris i 
					left join jenis j on i.id_jenis=j.id_jenis
					left join ruang r on i.id_ruang=r.id_ruang
					left join petugas p on i.id_jenis=p.id_petugas ");
				while($hasil=mysqli_fetch_array($sql)){;
				$kode_inventaris= $hasil['kode_inventaris'];
			?>
						<div class="modal animated rubberBand" id="myModal<?php echo $hasil['id_inventaris']; ?>" role="dialog">
                                    <div class="modal-dialog modals-default">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                                
						<div class="table-responsive">
                            <table id="data-table-basic" class="table table-striped">
                <tr>
                    <td>Nama Barang</td>
                    <td>:</td>
                    <td><?php echo $hasil['nama']; ?></td>
                </tr>
                <tr>
                    <td>Kondisi</td>
                    <td>:</td>
                    <td><?php echo $hasil['kondisi']; ?></td>
                </tr>
				<tr>
                    <td>Keterangan</td>
                    <td>:</td>
                    <td><?php echo $hasil['keterangan']; ?></td>
                </tr>
				<tr>
                    <td>Jumlah</td>
                    <td>:</td>
                    <td><?php echo $hasil['jumlah']; ?></td>
                </tr>
				<tr>
                    <td>Nama Jenis</td>
                    <td>:</td>
                    <td><?php echo $hasil['nama_jenis']; ?></td>
                </tr>
				<tr>
                    <td>Tanggal Register</td>
                    <td>:</td>
                    <td><?php echo $hasil['tanggal_register']; ?></td>
                </tr>
				<tr>
                    <td>Nama Ruang</td>
                    <td>:</td>
                    <td><?php echo $hasil['nama_ruang']; ?></td>
                </tr>
				<tr>
                    <td>Kode Inventaris</td>
                    <td>:</td>
                    <td><?php echo $hasil['kode_inventaris']; ?></td>
                </tr>
				<tr>
                    <td>Nama Petugas</td>
                    <td>:</td>
                    <td><?php echo $hasil['nama_petugas']; ?></td>
                </tr>
				
            </table>
											</div>
                                        </div>
                                    </div>
                                </div>
								</div>
								<?php
              };
            ?>

    <!-- End Footer area-->
    <!-- jquery
		============================================ -->
    <script src="tampilan/js/vendor/jquery-1.12.4.min.js"></script>
    <!-- bootstrap JS
		============================================ -->
    <script src="tampilan/js/bootstrap.min.js"></script>
    <!-- wow JS
		============================================ -->
    <script src="tampilan/js/wow.min.js"></script>
    <!-- price-slider JS
		============================================ -->
    <script src="tampilan/js/jquery-price-slider.js"></script>
    <!-- owl.carousel JS
		============================================ -->
    <script src="tampilan/js/owl.carousel.min.js"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="tampilan/js/jquery.scrollUp.min.js"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="tampilan/js/meanmenu/jquery.meanmenu.js"></script>
    <!-- counterup JS
		============================================ -->
    <script src="tampilan/js/counterup/jquery.counterup.min.js"></script>
    <script src="tampilan/js/counterup/waypoints.min.js"></script>
    <script src="tampilan/js/counterup/counterup-active.js"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="tampilan/js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- sparkline JS
		============================================ -->
    <script src="tampilan/js/sparkline/jquery.sparkline.min.js"></script>
    <script src="tampilan/js/sparkline/sparkline-active.js"></script>
    <!-- flot JS
		============================================ -->
    <script src="tampilan/js/flot/jquery.flot.js"></script>
    <script src="tampilan/js/flot/jquery.flot.resize.js"></script>
    <script src="tampilan/js/flot/flot-active.js"></script>
    <!-- knob JS
		============================================ -->
    <script src="tampilan/js/knob/jquery.knob.js"></script>
    <script src="tampilan/js/knob/jquery.appear.js"></script>
    <script src="tampilan/js/knob/knob-active.js"></script>
    <!--  Chat JS
		============================================ -->
    <script src="tampilan/js/chat/jquery.chat.js"></script>
    <!--  todo JS
		============================================ -->
    <script src="tampilan/js/todo/jquery.todo.js"></script>
	<!--  wave JS
		============================================ -->
    <script src="tampilan/js/wave/waves.min.js"></script>
    <script src="tampilan/js/wave/wave-active.js"></script>
    <!-- plugins JS
		============================================ -->
    <script src="tampilan/js/plugins.js"></script>
    <!-- Data Table JS
		============================================ -->
    <script src="tampilan/js/data-table/jquery.dataTables.min.js"></script>
    <script src="tampilan/js/data-table/data-table-act.js"></script>
    <!-- main JS
		============================================ -->
    <script src="tampilan/js/main.js"></script>
	<!-- tawk chat JS
		============================================ -->
    <script src="tampilan/js/tawk-chat.js"></script>
</body>

</html>