<?php
session_start();
if(!isset($_SESSION['username'])){
	die("<script>alert('silahkan login');
	document.location.href='hal_login.php' 
	</script>");
}
?>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>UJIKOM INVENTARIS</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="tampilan/img/favicon.ico">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/bootstrap.min.css">
    <!-- font awesome CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/font-awesome.min.css">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/owl.carousel.css">
    <link rel="stylesheet" href="tampilan/css/owl.theme.css">
    <link rel="stylesheet" href="tampilan/css/owl.transitions.css">
    <!-- meanmenu CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/meanmenu/meanmenu.min.css">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/animate.css">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/normalize.css">
	<!-- wave CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/wave/waves.min.css">
    <link rel="stylesheet" href="tampilan/css/wave/button.css">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- Notika icon CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/notika-custom-icon.css">
    <!-- Data Table JS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/jquery.dataTables.min.css">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/main.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/style.css">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="tampilan/css/responsive.css">
    <!-- modernizr JS
		============================================ -->
    <script src="tampilan/js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- Start Header Top Area -->
    <div class="header-top-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="logo-area">
                        <h2><font color="white"> <i class="notika-icon notika-app"></i> Inventarisir </font></h2>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="header-top-menu">
                        <ul class="nav navbar-nav notika-top-nav">
                            <li class="nav-item dropdown">
                                <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><span></span></a>
                                <div role="menu" class="dropdown-menu search-dd animated flipInX">
                                    <div class="search-input">
                                        <i class="notika-icon notika-left-arrow"></i>
                                        <input type="text" />
                                    </div>
                                </div>
                            </li>
						</ul>
                    </div>
            </div>
        </div>
    </div>
    <!-- End Header Top Area -->
    <!-- Mobile Menu start -->
    <div class="mobile-menu-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="mobile-menu">
                        <nav id="dropdown">
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Mobile Menu end -->
    <!-- Main Menu area start-->
    <div class="main-menu-area mg-tb-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul class="nav nav-tabs notika-menu-wrap menu-it-icon-pro">
                        <li><a href="coba2.php"><i class="notika-icon notika-house"></i> Dashboard </a>
                        </li>
                        <li ><a href="inventarisir.php"><i class="notika-icon notika-house"></i> Inventarisir </a>
                        </li>
                        <li ><a href="peminjaman.php"><i class="notika-icon notika-mail"></i> Peminjaman</a>
                        </li>
                        <li ><a href="pengembalian.php"><i class="notika-icon notika-edit"></i> Pengembalian</a>
                        </li>
                        <li class="active"><a href="laporan2.php"><i class="notika-icon notika-bar-chart"></i> Laporan Data </a>
                        </li>
                        <li ><a data-toggle="tab" href="#Referensi"><i class="notika-icon notika-windows"></i> Referensi</a>
                        </li>
                        <li><a data-toggle="tab" href="logout.php"><i class="notika-icon notika-form"></i>Logout</a>
                        </li>
                    </ul>
                    <div class="tab-content custom-menu-content">
                                               <div id="DB" class="tab-pane in notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="inventarisir.php">Data Peminjaman</a>
                                </li>
                                </li>
                            </ul>
                        </div>
						<div id="PB" class="tab-pane in notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="peminjaman.php">Peminjaman</a>
                                </li>
                                </li>
                            </ul>
                        </div>
						<div id="KB" class="tab-pane in notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="pengembalian.php">Pengembalian</a>
                                </li>
                                </li>
                            </ul>
                        </div>
						<div id="Laporan" class="tab-pane in notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="inventarisir.php">Data Barang</a>
                                </li>
                                <li><a href="laporan_peminjaman.html">Data Peminjaman</a>
                                </li>
                                <li><a href="ua.html">User Akun</a>
                                </li>
                                </li>
                            </ul>
                        </div>
                        <div id="Referensi" class="tab-pane notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li class="active"><a href="jenis.php">Jenis</a></li>
								<li><a href="level.php">Level</a></li>
								<li><a href="ruang.php">Ruang</a></li>
								<li><a href="pegawai.php">Pegawai</a></li>
								<li><a href="petugas.php">Petugas</a></li>
                            </ul>
                        </div>
                        <div id="UA" class="tab-pane notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="pengaturan_admin.php">Pengaturan Akun</a>
                                </li>
                                <li><a href="logout.php">Logout</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Main Menu area End-->
	<!-- Breadcomb area Start-->
	
	<!-- Breadcomb area End-->
    <!-- Data Table area Start-->
    <div class="data-table-area">
        <div class="container">
            <div class="row">
                <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="btn-demo-notika mg-t-30">
						<div class="notika-btn-hd">
							</div>
                        <div class="material-design-btn">
						
						
						<h3 class="box-title">Laporan Data Peminjaman perTanggal</h3><br/>
          </div><!-- /.box-header -->
          <div class="box-body">
            <form method="POST" action="laporan2.php">
            Mulai tanggal <input type="date" name="tgl1"/>
            akhir tanggal<input type="date" name="tgl2"/>
            <input type="submit" value="cari">
            </form>
            </br>
            <p align="left"><a href="laporan_pptgl.php?tgl1=<?php echo $_POST['tgl1']?>&tgl2=<?php echo $_POST['tgl2']?>" target="_blank" class="btn btn-primary">Export Data PDf</a></p>
        <br/>
          <table id="data-table-basic" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Kode pinjam</th>
										<th>Tgl Pinjam </th>
										<th>Tgl Kembali </th>
                                        <th>Kode Barang</th>
                                        <th>Nama Barang</th>
                                        <th>Jumlah Pinjam</th>
                                        <th>Keterangan</th>
                                        <th>Nama Pegawai</th>
                                    </tr>
                                </thead>
                                <tbody>
                 <?php
                 error_reporting(0);
            $no = 1;
				include "koneksi.php";
				$sql=mysqli_query($connect,"select * from peminjaman pe 
				left join pegawai on pe.id_pegawai=pegawai.id_pegawai
				left join detail_pinjam dp on dp.kd_pinjam=pe.kd_pinjam 
				join inventaris on dp.id_inventaris = inventaris.id_inventaris where pe.tanggal_pinjam BETWEEN '$_POST[tgl1]' and '$_POST[tgl2]'" );
				while($hasil=mysqli_fetch_array($sql)){;
				
              ?>
                <tr>
			<td align="center"><?php echo $hasil["kode"]; ?></td>
			<td align="center"><?php echo $hasil["tanggal_pinjam"]; ?></td>
			<td align="center"><?php echo $hasil["tanggal_kembali"]; ?></td>
			<td align="center"><?php echo $hasil["kode_inventaris"]; ?></td>
			<td align="center"><?php echo $hasil["nama"]; ?></td>
			<td align="center"><?php echo $hasil["jumlah_pinjam"]; ?></td>
			<td align="center"><?php echo $hasil["status_peminjaman"]; ?></td>
			<td align="center"><?php echo $hasil["nama_pegawai"]; ?></td>
										
                                    </tr>
									<?php
              } $connect->close(); 
            ?>
             </tbody>
          
         </table>
       </div><!-- /.box-body -->
     
						
						
						</div>
                    </div>
                </div>
            </div>
    <div class="footer-copyright-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="footer-copy-right">
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Footer area-->
    <!-- jquery
		============================================ -->
    <script src="tampilan/js/vendor/jquery-1.12.4.min.js"></script>
    <!-- bootstrap JS
		============================================ -->
    <script src="tampilan/js/bootstrap.min.js"></script>
    <!-- wow JS
		============================================ -->
    <script src="tampilan/js/wow.min.js"></script>
    <!-- price-slider JS
		============================================ -->
    <script src="tampilan/js/jquery-price-slider.js"></script>
    <!-- owl.carousel JS
		============================================ -->
    <script src="tampilan/js/owl.carousel.min.js"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="tampilan/js/jquery.scrollUp.min.js"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="tampilan/js/meanmenu/jquery.meanmenu.js"></script>
    <!-- counterup JS
		============================================ -->
    <script src="tampilan/js/counterup/jquery.counterup.min.js"></script>
    <script src="tampilan/js/counterup/waypoints.min.js"></script>
    <script src="tampilan/js/counterup/counterup-active.js"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="tampilan/js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- sparkline JS
		============================================ -->
    <script src="tampilan/js/sparkline/jquery.sparkline.min.js"></script>
    <script src="tampilan/js/sparkline/sparkline-active.js"></script>
    <!-- flot JS
		============================================ -->
    <script src="tampilan/js/flot/jquery.flot.js"></script>
    <script src="tampilan/js/flot/jquery.flot.resize.js"></script>
    <script src="tampilan/js/flot/flot-active.js"></script>
    <!-- knob JS
		============================================ -->
    <script src="tampilan/js/knob/jquery.knob.js"></script>
    <script src="tampilan/js/knob/jquery.appear.js"></script>
    <script src="tampilan/js/knob/knob-active.js"></script>
    <!--  Chat JS
		============================================ -->
    <script src="tampilan/js/chat/jquery.chat.js"></script>
    <!--  todo JS
		============================================ -->
    <script src="tampilan/js/todo/jquery.todo.js"></script>
	<!--  wave JS
		============================================ -->
    <script src="tampilan/js/wave/waves.min.js"></script>
    <script src="tampilan/js/wave/wave-active.js"></script>
    <!-- plugins JS
		============================================ -->
    <script src="tampilan/js/plugins.js"></script>
    <!-- Data Table JS
		============================================ -->
    <script src="tampilan/js/data-table/jquery.dataTables.min.js"></script>
    <script src="tampilan/js/data-table/data-table-act.js"></script>
    <!-- main JS
		============================================ -->
    <script src="tampilan/js/main.js"></script>
	<!-- tawk chat JS
		============================================ -->
    <script src="tampilan/js/tawk-chat.js"></script>
</body>

</html>