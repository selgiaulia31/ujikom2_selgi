<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->

<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>ujikom inventaris</title>
    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8" />
    <meta name="keywords" content="Toon Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script>
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }

    </script>
    <!-- //Meta tag Keywords -->

    <!-- Custom-Files -->
    <link rel="stylesheet" href="tampilan_awal/css/bootstrap.css">
    <!-- Bootstrap-Core-CSS -->
    <link rel="stylesheet" href="tampilan_awal/css/style.css" type="text/css" media="all" />
    <!-- Style-CSS -->
    <!-- font-awesome-icons -->
    <link href="tampilan_awal/css/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome-icons -->
    <!-- /Fonts -->
    <link href="//fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900" rel="stylesheet">
    <!-- //Fonts -->
	
	<!-- Custom Theme files -->
<link href='tampilan_login/css/bootstrap.min.css' media='all' rel='stylesheet'>
<link href="tampilan_login/css/style.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="tampilan_login/css/animate.min.css"> 
<!-- //Custom Theme files -->
<!-- web font -->
<link href='tampilan_login///fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'><!--web font-->
<!-- //web font --> 


</head>

<body>
    <!-- mian-content -->
    <section class="main-content" id="home">

        <!-- header -->
        <header>
            <div class="container-fluid px-lg-5">
                <!-- nav -->
                <nav>
                    <div class="logo-w3ls" id="logo-w3ls">
                        <h1>
                            <a href="index.php"><span>inventaris</span></a>
                        </h1>

                    </div>

                   <ul class="menu">
                        <li class="mr-lg-4 mr-3"><a href="index.php">INVENTARIS SEKOLAH</a></li>
                        <li class="mr-lg-4 mr-3"><a href="hal_login.php.php">Login</a></li>
                        
                    </ul>
                </nav>
                <!-- //nav -->
            </div>
        </header>
        <!-- //header -->


        <!-- //header -->
                <h3 class="tittle text-center"><span class="sub-tittle"></span></h3>
				<img width="100%" height="100%" src="tampilan_awal/images/banner4.jpg">
		
			</div>
        </div>
    </section>
    <!--// banner-slider -->
    <!--/ab -->
    <section class="about py-lg-5 py-md-5 py-3">
         <section class="about py-lg-5 py-md-5 py-3">
        <div class="agileits">
		<h1>Silahkan Login Untuk Meminjam Barang</h1>
		<div class="w3-agileits-info">
			<form class='form animate-form' id='form1' action="login.php" method="post">
				<p class="w3agileits">Login Here</p>
				<div class='form-group has-feedback w3ls'>
					<label class='control-label sr-only' for='username'>Username</label> 
					<input class='form-control' id='username' name='username' placeholder='Username' type='text' autofocus>
					<span class='glyphicon glyphicon-ok form-control-feedback'></span>
				</div>
				<div class='form-group has-feedback agile'>
					<label class='control-label sr-only' for='password'>Password</label> 
					<input class='form-control w3l' id='password' name='password' placeholder='Password' type='password'><span class='glyphicon glyphicon-ok form-control-feedback'></span>
				</div>
				<div class='submit w3-agile'>
					<input class='btn btn-lg' type='submit' value='SUBMIT'>
				</div>
			</form>
		</div>	
	</div>	
	<!-- //agileits -->
	<!-- copyright -->
	<div class="w3-agile-copyright">
	</div>
	
    </section>
    </section>
 
</body>

</html>
