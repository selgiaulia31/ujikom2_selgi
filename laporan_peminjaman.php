<?php ob_start(); ?>
<html>
<head>
  <title>Cetak PDF</title>
    
   <style>
   table {border-collapse:collapse; table-layout:fixed;width:700%;  }
   table td {word-wrap:break-word;width: 15%; height:5%;}
   </style>
</head>
<body>
  
<h1 style="text-align: center; color:red;">Data peminjaman</h1>
<table border="1" width="100%">
<tr>
  <th>Kode Inventaris</th>
  <th>Nama</th>
  <th>jumlah pinjam</th>
  <th>tanggal pinjam</th>
  <th>tanggal kembali</th>
  <th>status peminjaman</th>
</tr>
<?php
// Load file koneksi.php
include "koneksi.php";
 
$query = "select * from peminjaman i 
											LEFT JOIN detail_pinjam j on i.id_detail_peminjaman=j.id_detail_pinjam join inventaris on inventaris.id_inventaris = j.id_inventaris"; // Tampilkan semua data gambar
$sql = mysqli_query($connect, $query); // Eksekusi/Jalankan query dari variabel $query
$row = mysqli_num_rows($sql); // Ambil jumlah data dari hasil eksekusi $sql
 
if($row > 0){ // Jika jumlah data lebih dari 0 (Berarti jika data ada)
    while($data = mysqli_fetch_array($sql)){ // Ambil semua data dari hasil eksekusi $sql
        echo "<tr>";
        echo "<td>".$data['kode_inventaris']."</td>";
		echo "<td>".$data['nama']."</td>";
		echo "<td>".$data['jumlah_pinjam']."</td>";
		echo "<td>".$data['tanggal_pinjam']."</td>";
		echo "<td>".$data['tanggal_kembali']."</td>";
		echo "<td>".$data['status_peminjaman']."</td>";
        echo "</tr>";
    }
}else{ // Jika data tidak ada
    echo "<tr><td colspan='4'>Data tidak ada</td></tr>";
}
?>
</table>

</body>
</html>
<?php
$html = ob_get_contents();
ob_end_clean();
        
require_once('html2pdf/html2pdf.class.php');
$pdf = new HTML2PDF('P','A4','en');
$pdf->WriteHTML($html);
$pdf->Output('Data peminjaman.pdf', 'D');
?>
