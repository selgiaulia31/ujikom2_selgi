<?php ob_start(); ?>
<html>
<head>
  <title>Cetak PDF</title>
    
   <style>
   table {border-collapse:collapse; table-layout:fixed;width:700%; center; }
   table td {word-wrap:break-word;width: 15%; height:5%;text-align: center;}
   </style>
</head>
<body>
  
<h1 style="text-align: center; color:red;">Data peminjaman</h1>
<table border="1" width="100%">
<tr>
	<th style="text-align: center;">Username</th>
	<th style="text-align: center;">Nama Petugas</th>
	<th style="text-align: center;">Level </th>
</tr>
<?php
// Load file koneksi.php
include "koneksi.php";
 
$query = "select * from petugas p left join level l on p.id_level=l.id_level"; // Tampilkan semua data gambar
$sql = mysqli_query($connect, $query); // Eksekusi/Jalankan query dari variabel $query
$row = mysqli_num_rows($sql); // Ambil jumlah data dari hasil eksekusi $sql
 
if($row > 0){ // Jika jumlah data lebih dari 0 (Berarti jika data ada)
    while($data = mysqli_fetch_array($sql)){ // Ambil semua data dari hasil eksekusi $sql
        echo "<tr>";
        echo "<td>".$data['username']."</td>";
		echo "<td>".$data['nama_petugas']."</td>";
		echo "<td>".$data['nama_level']."</td>";
        echo "</tr>";
    }
}else{ // Jika data tidak ada
    echo "<tr><td colspan='4'>Data tidak ada</td></tr>";
}
?>
</table>

</body>
</html>
<?php
$html = ob_get_contents();
ob_end_clean();
        
require_once('html2pdf/html2pdf.class.php');
$pdf = new HTML2PDF('P','A5','en');
$pdf->WriteHTML($html);
$pdf->Output('Data peminjaman.pdf', 'D');
?>
